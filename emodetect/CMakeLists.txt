FIND_PACKAGE(OpenCV REQUIRED)
add_library(emodetect SHARED emodetect.cpp)
target_link_libraries(emodetect ${OpenCV_LIBS})

