#ifndef EMODETECT_H
#define EMODETECT_H

#include <iostream>
#include <time.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

namespace ed
{
    // defaults
    #define DEFAULT_CAPTURE 0
    #define DEFAULT_CASCADE_FACE "../../cascades/haarcascade_frontalface_default.xml"
    #define DEFAULT_CASCADE_HAPPY "../../cascades/haarcascade_smile.xml"
    #define DEFAULT_CASCADE_SAD ""
    #define DEFAULT_CASCADE_ANGER ""
    #define DEFAULT_CASCADE_FEAR ""
    #define DEFAULT_SCALE 1.0
    #define DEFAULT_DRAWIMAGE false
    
    // errors
    #define ERROR_NOFRAME 1
    #define ERROR_CAPTURE 2
    #define ERROR_CASCADE 3

	class Emodetect
	{
	public:
		enum CalibrationType
		{
			CA_IMAGE_WIDTH,
			CA_CALIBRATED_BORDERS,
			CA_SIMPLE_ADAPTIVE_BORDERS,
			CA_GAUSSIAN_ADAPTIVE_BORDERS
		};

		Emodetect();
		~Emodetect();

		int init(bool drawimage = DEFAULT_DRAWIMAGE,
			double scale = DEFAULT_SCALE,
            int input = DEFAULT_CAPTURE,
			const std::string& faceCascadePath = DEFAULT_CASCADE_FACE,
			const std::string& happyCascadePath = DEFAULT_CASCADE_HAPPY,
			const std::string& sadCascadePath = DEFAULT_CASCADE_SAD,
			const std::string& angerCascadePath = DEFAULT_CASCADE_ANGER,
			const std::string& fearCascadePath = DEFAULT_CASCADE_FEAR);

		void setCalibrationType(CalibrationType type);
		bool needsCalibration() const;

		void destroy();
		void calibrate();

		int update();

		int getHappy() const;
		int getSad() const;
		int getAnger() const;
		int getFear() const;

		int getNeighbours() const;
	private:
		int mNeighbours;

	private:
		void detect(cv::Mat& img);
		unsigned int detectOnlyNeighbours(cv::Mat& img);

		void preprocess(cv::Mat& img, cv::Mat& outSmall, cv::Mat& outBig);
		void detectFaces(cv::Mat& img, std::vector<cv::Rect>& faces);
		void detectEmotion(
			cv::Mat& nestedImg,
			cv::CascadeClassifier& nestedCascade,
			unsigned int& foundNeighbours);

		int getNewFrame(cv::Mat& frameCopy);
		void draw(cv::Mat& frameCopy);

		const static cv::Scalar msColors[];

		clock_t mLastUpdate;
		CalibrationType mCalib;
		bool mNeedsCalibration;
		int mMindiv;
		std::deque<unsigned int> mMinBuffer, mMaxBuffer;
		double mMinEpsilon, mMaxEpsilon;
		unsigned int mMinNeighbours, mMaxNeighbours;

		// Emotion variables
		int mHappy;
		int mSad;
		int mAnger;
		int mFear;
		cv::Rect mFaceFallback;

		// Parameters
		cv::CascadeClassifier mFaceCascade;
		cv::CascadeClassifier mHappyCascade;
		//TODO: implement
		//static CascadeClassifier ed_sadCascade;
		//static CascadeClassifier ed_angerCascade;
		//static CascadeClassifier ed_fearCascade;
		CvCapture* mCapture;
		double mScale;
		bool mDrawImage;
	};
}

#endif /* EMODETECT_H */
