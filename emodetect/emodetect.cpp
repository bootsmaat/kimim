#include "emodetect.h"
#include <cctype>
#include <iostream>
#include <iterator>
using namespace ed;

const cv::Scalar Emodetect::msColors[] = {
	CV_RGB(0,0,255),
	CV_RGB(0,128,255),
	CV_RGB(0,255,255),
	CV_RGB(0,255,0),
	CV_RGB(255,128,0),
	CV_RGB(255,255,0),
	CV_RGB(255,0,0),
	CV_RGB(255,0,255)};

#define ED_FRAME_INTERLEAVE 1
#define ED_FACESCALE 2
#define ED_DEC_RATE 0.9f
#define ED_IMGNAME "result"

// formula for min_neighbors calculation
#define ED_MINCALC(x,y) ((x)*(x)/y)
#define ED_MAXCALC(x,y) (2*ED_MINCALC(x,y))
#define DEFAULT_ED_MINDIV 200
static int minHits = 0;
static int maxHits = 0;
Emodetect::Emodetect()
{
    mHappy = 0;
    mSad = 0;
    mAnger = 0;
    mFear = 0;

	mMindiv = DEFAULT_ED_MINDIV;
	mScale = DEFAULT_SCALE;
	mDrawImage = DEFAULT_DRAWIMAGE;
	mFaceFallback = cv::Rect(0,0,0,0);

	//setCalibrationType(CA_GAUSSIAN_ADAPTIVE_BORDERS);
	setCalibrationType(CA_CALIBRATED_BORDERS);

	mMinNeighbours = std::numeric_limits<unsigned int>::max();
	mMaxNeighbours = 0;

	for (int i = 0; i < 30; ++i) mMaxBuffer.push_back(rand() % 500 + 500);
	for (int i = 0; i < 30; ++i) mMinBuffer.push_back(rand() % 500);
	mMinEpsilon = 1;
	mMaxEpsilon = 1;
}
Emodetect::~Emodetect()
{
	destroy();
}


/********************
 * Get functions. Call them to get the values from the emotion variables
 ********************/
int Emodetect::getHappy() const
{
    return mHappy;
}
int Emodetect::getSad() const
{
    return mSad;
}
int Emodetect::getAnger() const
{
    return mAnger;
}
int Emodetect::getFear() const
{
    return mFear;
}

void Emodetect::setCalibrationType(CalibrationType type)
{
	mCalib = type;
	switch (type)
	{
	case CA_SIMPLE_ADAPTIVE_BORDERS:
	case CA_GAUSSIAN_ADAPTIVE_BORDERS:
		mNeedsCalibration = false;
		break;
	case CA_CALIBRATED_BORDERS:
	case CA_IMAGE_WIDTH:
		mNeedsCalibration = true;
		break;
	}
}
bool Emodetect::needsCalibration() const
{
	return mNeedsCalibration;
}


/********************
 * The Init Function, used to initialize cascades and streams and to set
 * initial variables. Default values from the header file are applied
 * if the value is missing (empty string, scale<1, drawImage<0).
 * Then the capture for a cam (if input is digit) or an avi file (if
 * input is some file path) is initialized. Return codes and Defaults
 * can be found in the header file.
 ********************/
int Emodetect::init(bool drawimage, double scale,
            int input,
			const std::string& faceCascadePath,
			const std::string& happyCascadePath,
			const std::string& sadCascadePath,
			const std::string& angerCascadePath,
			const std::string& fearCascadePath)
{
    #ifdef _DEBUG
    std::cout << ">>> Entering edInit..." << std::endl;
    const clock_t clk = clock();
    #endif


    #ifdef _DEBUG
    std::cout << "     > Setting vars..." << std::endl;
    #endif
    // reset vars and defaults
    mHappy = 0;
    mSad = 0;
    mAnger = 0;
    mFear = 0;
	mMindiv = DEFAULT_ED_MINDIV;
	mScale = scale;
	mDrawImage = drawimage;

    #ifdef _DEBUG
    std::cout << "       var input: " << input << std::endl;
    std::cout << "       var faceCascadePath: " <<  faceCascadePath << std::endl;
    std::cout << "       var happyCascadePath: " << happyCascadePath << std::endl;
    std::cout << "       var sadCascadePath: " <<   sadCascadePath << std::endl;
    std::cout << "       var angerCascadePath: " << angerCascadePath << std::endl;
    std::cout << "       var fearCascadePath: " <<  fearCascadePath << std::endl;
    std::cout << "       var ed_scale: " << mScale << std::endl;
    std::cout << "       var ed_drawImage: " << mDrawImage << std::endl;
    #endif


    // get capture
    #ifdef _DEBUG
    std::cout << "      > Getting capture... ";
    #endif
    mCapture = cvCaptureFromCAM(input);
    /*
    if (isdigit(input.c_str()[0]) && input.c_str()[1] == '\0')
    {
        #ifdef _DEBUG
        std::cout << "from Cam." << std::endl;
        #endif
        mCapture = cvCaptureFromCAM(input.c_str()[0] - '0');
    }
    else
    {
        mCapture = cvCaptureFromAVI(input.c_str());
        #ifdef _DEBUG
        std::cout << "from AVI." << std::endl;
        #endif
    }
    */

    if (!mCapture)
    {
        std::cerr << "ERROR: Capture from " << input
             << " didn't work!" << std::endl;
        return ERROR_CAPTURE;
    }

    #ifdef _DEBUG
    std::cout << "      > Loading Cascades... " << std::endl;
    #endif
    // load cascades
    if (!mFaceCascade.load(faceCascadePath))
    {
        std::cerr << "ERROR: could not load face cascade " << std::endl;
        return ERROR_CASCADE;
    }
    if (!mHappyCascade.load(happyCascadePath))
    {
        std::cerr << "ERROR: could not load happiness cascade "
             << happyCascadePath << std::endl;
        return ERROR_CASCADE;
    }
/*
    // TODO: implement
    if (!ed_sadCascade.load(sadCascadePath))
    {
        cerr << "ERROR: could not load sadness cascade "
             << sadCascadePath << endl;
        return ERROR_CASCADE;
    }
    if (!ed_angerCascade.load(angerCascadePath))
    {
        cerr << "ERROR: could not load anger cascade "
             << angerCascadePath << endl;
        return ERROR_CASCADE;
    }
    if (!cascade.load(fearCascadePath))
    {
        cerr << "ERROR: could not load fear cascade "
             << fearCascadePath << endl;
        return ERROR_CASCADE;
    }
*/

    // create window if ed_drawImage flag is set
    if (mDrawImage) cvNamedWindow(ED_IMGNAME, cv::WINDOW_AUTOSIZE);

    #ifdef _DEBUG
    std::cout << "<<< Exiting edInit. Used time: ";
    std::cout << float( clock() - clk ) / CLOCKS_PER_SEC << std::endl;
    #endif

    return 0;
}

int Emodetect::getNeighbours() const
{
	return mNeighbours;
}


void Emodetect::destroy()
{
    #ifdef _DEBUG
    std::cout << ">>> Called edDestroy." << std::endl;
    #endif

    if (mDrawImage) cvDestroyWindow("result");
    cvReleaseCapture(&mCapture);
}


/********************
 * This is called by detectAndDraw(). It tries to find nested objects
 * (emotions) inside the borders of an existing object (face). Found
 * neighbors are used to measure the intensity of the emotion. With
 * the given max and min values, a percentage is calculated and set.
 ********************/
void Emodetect::detectEmotion(
    cv::Mat& nestedImg,
	cv::CascadeClassifier& nestedCascade,
	unsigned int& foundNeighbours)
{
#ifdef _DEBUG
    std::cout << ">>> Entering detectEmotion..." << std::endl;
    const clock_t clk = clock();
#endif

    // find nested object
    std::vector<cv::Rect> nestedObjects;
    nestedCascade.detectMultiScale(nestedImg, nestedObjects,
        1.02, 0, 0
        //|CASCADE_FIND_BIGGEST_OBJECT
        //|CASCADE_DO_ROUGH_SEARCH
        //|CASCADE_DO_CANNY_PRUNING
        | cv::CASCADE_SCALE_IMAGE
        ,
        cv::Size(0,0)
    );
	foundNeighbours = (unsigned int)nestedObjects.size();
	mNeighbours = foundNeighbours;

#ifdef _DEBUG
	std::cout << "      > Got found_neighbors: " << foundNeighbours << std::endl;
	std::cout << "<<< Exiting detectEmotion.Used time: ";
	std::cout << float(clock() - clk) / CLOCKS_PER_SEC << std::endl;
#endif
}

float clamp(float v, float min, float max)
{
	return std::max(min, std::min(max, v));
}

float mapLinear(unsigned int x, unsigned int a, unsigned int b)
{
	if (a == b) return (float)x - a;
	return ((float)x - a) / (b - a);
}

template<typename T>
void getStandardStatistic(const std::deque<T>& data, double& mean, double& variance)
{
	typename std::deque<T>::const_iterator iter, iend;
	iter = data.begin(); iend = data.end();
	mean = 0;
	for (; iter != iend; ++iter)
		mean += (double)*iter;
	mean /= data.size();

	iter = data.begin();
	variance = 0;
	for (; iter != iend; ++iter)
	{
		double v = ((double)*iter - mean);
		variance += v*v;
	}
	variance /= data.size() - 1;
	variance = std::sqrt(variance);
}

void Emodetect::preprocess(cv::Mat& img, cv::Mat& small, cv::Mat& big)
{
	cv::Mat gray;
	// make gray and resize
#ifdef _DEBUG
	std::cout << "      > Editing Image... " << std::endl;
#endif
	cvtColor(img, gray, cv::COLOR_BGR2GRAY);
	resize(gray, small, small.size(), 0, 0, cv::INTER_LINEAR);
	equalizeHist(small, small);
	resize(gray, big, big.size(), 0, 0, cv::INTER_LINEAR);
	equalizeHist(big, big);
}

void Emodetect::detectFaces(cv::Mat& img, std::vector<cv::Rect>& faces)
{
	// find faces
#ifdef _DEBUG
	std::cout << "      > Detecting Faces..." << std::endl;
#endif
	mFaceCascade.detectMultiScale(img, faces,
		1.1,    // increase search scale by 10% each pass
		5,      // drop groups with fewer than 5 detections
		0       // flags
		//|CASCADE_FIND_BIGGEST_OBJECT
		//|CASCADE_DO_ROUGH_SEARCH
		| cv::CASCADE_SCALE_IMAGE
		,
		cv::Size(0, 0)     // 0,0: use xml default for smallest search scale
		);

	// find emotions in each face
	if (faces.empty() && mFaceFallback.area() > 0)
		faces.push_back(mFaceFallback);
}

/********************
 * This is called by edUpdate(). The function is given an image, which
 * is duplicated twice: One big image (only scale applied) and one small
 * image (scale and facescale applied). The small image is searched for
 * faces. Each detected face is then extracted from the big image and
 * searched for each emotion.
 * Searching for faces takes a lot of effort and face detection is good,
 * even with low resolution. So you may want to decrease the resolution
 * of the small image.
 * If an image is drawn, emotion intensity is displayed in it.
 ********************/
void Emodetect::detect(cv::Mat& img)
{
    #ifdef _DEBUG
    std::cout << ">>> Entering detectAndDraw..." << std::endl;
    const clock_t clk = clock();
    #endif

	cv::Mat big( // big image for emotion detection
		cvRound(img.rows / mScale),
		cvRound(img.cols / mScale),
		CV_8UC1
		);
	cv::Mat small( // small image for face detection
		cvRound(big.rows / ED_FACESCALE),
		cvRound(big.cols / ED_FACESCALE),
		CV_8UC1
		);
	preprocess(img, big, small);

	std::vector<cv::Rect> faces;
	detectFaces(small, faces);

	std::vector<cv::Rect>::iterator iter, iend;
	iter = faces.begin(); iend = faces.end();
	for (int i = 0; iter != iend; iter++, i++)
    {
        #ifdef _DEBUG
        std::cout << "      > Detecting emotions on face " << i << "..." << std::endl;
        #endif

        cv::Mat faceImg;
		
		cv::Rect faceRect = *iter;

        // smile/happy detection
        iter->x *= ED_FACESCALE;
        iter->y *= ED_FACESCALE;
        iter->width *= ED_FACESCALE;
        iter->height *= ED_FACESCALE;

        // faces have a higher resolution, so emotions can be found
        // draw rectangle around face
        if (mDrawImage)
            rectangle(img,
                cvPoint(cvRound(iter->x*mScale), cvRound(iter->y*mScale)),
                cvPoint(cvRound((iter->x + iter->width-1)*mScale),
                    cvRound((iter->y + iter->height-1)*mScale)),
                msColors[i%8], 3, 8, 0);


        const int half_height=cvRound((float)iter->height/2);
        iter->y += half_height;
        iter->height = half_height;
		faceImg = big(*iter);

		unsigned int foundNeighbours = 0;
		detectEmotion(faceImg, mHappyCascade, foundNeighbours);

		int happy = 0;
		if (foundNeighbours > 0)
		{
			// map to happiness value
			switch (mCalib)
			{
			case CA_IMAGE_WIDTH:
			{
				// get new min/max neighbors for emotion detection
				// (width*3/4 mit scale 1.5 bisher das beste ergebnis)
				int minNeighbours = ED_MINCALC(iter->width, mMindiv);
				int maxNeighbours = ED_MAXCALC(iter->width, mMindiv);
#ifdef _DEBUG
				std::cout << "        var ed_min: " << minNeighbours << std::endl;
				std::cout << "        var ed_max: " << maxNeighbours << std::endl;
#endif
				float v = mapLinear(foundNeighbours, minNeighbours, maxNeighbours);
				happy = (int)clamp(100 * v, 0.f, 100.f);
				break;
			}
			case CA_CALIBRATED_BORDERS:
			{
				float v = mapLinear(foundNeighbours,
					mNeedsCalibration ? 200 : mMinNeighbours,
					mNeedsCalibration ? 1000 : mMaxNeighbours);
				happy = (int)clamp(100 * v, 0.f, 100.f);
				break;
			}
			case CA_SIMPLE_ADAPTIVE_BORDERS:
			{
				mMinNeighbours = std::min(mMinNeighbours, (unsigned int)foundNeighbours);
				mMaxNeighbours = std::max(mMaxNeighbours, (unsigned int)foundNeighbours);
				float v = mapLinear(foundNeighbours, mMinNeighbours, mMaxNeighbours);
				happy = (int)clamp(100 * v, 0.f, 100.f);
				break;
			}
			case CA_GAUSSIAN_ADAPTIVE_BORDERS:
			{
				double minMean, maxMean, minVar, maxVar;
				getStandardStatistic(mMinBuffer, minMean, minVar);
				getStandardStatistic(mMaxBuffer, maxMean, maxVar);

				

				bool hitMin = false;
				bool hitMax = false;
				if (foundNeighbours < minMean
				||  std::abs(minMean - foundNeighbours) < mMinEpsilon * minVar)
				{
					hitMin = true;
				}
				else
				{
					mMinEpsilon = mMinEpsilon < 1 ? 1 : mMinEpsilon * 1.15;
				}
				
				if (foundNeighbours > maxMean
				|| std::abs(maxMean - foundNeighbours) < mMaxEpsilon * maxVar)
				{
					hitMax = true;
				}
				else
				{
					mMaxEpsilon = mMaxEpsilon < 1 ? 1 : mMaxEpsilon * 1.15;
				}

				if (foundNeighbours < minMean)
					hitMax = false;
				if (foundNeighbours > maxMean)
					hitMin = false;

				if (!hitMax && hitMin)
				{
					mMinBuffer.pop_front();
					mMinBuffer.push_back(foundNeighbours);
					mMinEpsilon *= 0.5;
					minHits++;
				}
				if (hitMax && !hitMin)
				{
					mMaxBuffer.pop_front();
					mMaxBuffer.push_back(foundNeighbours);
					mMaxEpsilon *= 0.5;
					maxHits++;
				}

				mMinNeighbours = (unsigned int) minMean;
				mMaxNeighbours = (unsigned int) maxMean;

				float v = mapLinear(foundNeighbours, mMinNeighbours, mMaxNeighbours);
				happy = (int)clamp(100 * v, 0.f, 100.f);
				break;
			}
			}
		}

		// only assign new value if larger, else emotion decays
		mHappy = happy > mHappy ? happy : mHappy;

		// if this round a face has been found, cache it!
		if (faceRect.area() > 0)
			mFaceFallback = faceRect;

        #ifdef _DEBUG
        std::cout << "        var happyness: " << mHappy << std::endl;
        #endif

        // sad detection 
        // TODO: implementieren
        mSad = 0;
/*
        r2 = r;
        r2->height = half_height;
        faceImg = smallImg(*r2);
        detectEmotion(&faceImg, &ed_sadCascade, &sad, ed_min, ed_max);
*/

        // anger detection
        // TODO: implementieren
        mAnger = 0;

        // fear detection
        // TODO: implementieren
        mFear = 0;
    }


    #ifdef _DEBUG
    std::cout << "<<< Exiting detect. Used time: ";
    std::cout << float( clock() - clk ) / CLOCKS_PER_SEC << std::endl;
    #endif
}

unsigned int Emodetect::detectOnlyNeighbours(cv::Mat& img)
{
	unsigned int foundNeighbours = 0;

	cv::Mat big( // big image for emotion detection
		cvRound(img.rows / mScale),
		cvRound(img.cols / mScale),
		CV_8UC1
		);
	cv::Mat small( // small image for face detection
		cvRound(big.rows / ED_FACESCALE),
		cvRound(big.cols / ED_FACESCALE),
		CV_8UC1
		);
	preprocess(img, big, small);

	std::vector<cv::Rect> faces;
	detectFaces(small, faces);

	std::vector<cv::Rect>::iterator iter, iend;
	iter = faces.begin(); iend = faces.end();
	for (; iter != iend; iter++)
	{
		cv::Mat faceImg;

		cv::Rect faceRect = *iter;

		// smile/happy detection
		iter->x *= ED_FACESCALE;
		iter->y *= ED_FACESCALE;
		iter->width *= ED_FACESCALE;
		iter->height *= ED_FACESCALE;

		const int half_height = cvRound((float)iter->height / 2);
		iter->y += half_height;
		iter->height = half_height;
		faceImg = big(*iter);

		detectEmotion(faceImg, mHappyCascade, foundNeighbours);

		if (foundNeighbours > 0)
			break;
	}
	return foundNeighbours;
}

void Emodetect::draw(cv::Mat& img)
{
#ifdef DEBUG
	std::cout << "      > drawing image..." << std::endl;
#endif
	const float intensityHappy = (float)mHappy / 100;
	const int happyX = img.cols * 1 / 4 / 10;
	rectangle(img,
		cvPoint(0, img.rows),
		cvPoint(happyX, img.rows - cvRound(
		(float)img.rows * intensityHappy)),
		msColors[0], -1);
	const float intensitySad = (float)mSad / 100;
	const int sadX = img.cols * 2 / 4 / 10;
	rectangle(img,
		cvPoint(happyX, img.rows),
		cvPoint(sadX, img.rows - cvRound(
		(float)img.rows * intensitySad)),
		msColors[2], -1);
	const float intensityAnger = (float)mAnger / 100;
	const int angerX = img.cols * 2 / 4 / 10;
	rectangle(img,
		cvPoint(sadX, img.rows),
		cvPoint(angerX, img.rows - cvRound(
		(float)img.rows * intensityAnger)),
		msColors[4], -1);
	const float intensityFear = (float)mFear / 100;
	const int fearX = img.cols * 4 / 4 / 10;
	rectangle(img,
		cvPoint(angerX, img.rows),
		cvPoint(fearX, img.rows - cvRound(
		(float)img.rows * intensityFear)),
		msColors[6], -1);

	// show image
	imshow(ED_IMGNAME, img);
}

int Emodetect::getNewFrame(cv::Mat& frameCopy)
{
	cv::Mat frame;
	IplImage* img;
	// remove old frames first (sleep problem)
	for (int i = 0; i<ED_FRAME_INTERLEAVE; i++)
		cvQueryFrame(mCapture);
	img = cvQueryFrame(mCapture);
	frame = cv::cvarrToMat(img);

	if (frame.empty())
		return ERROR_NOFRAME;

	// copy frame, flip if origin isn't top left
	if (img->origin == IPL_ORIGIN_TL)
		frame.copyTo(frameCopy);
	else
		flip(frame, frameCopy, 0);
	return 0;
}

/********************
 * The update function has to be called to get a new input frame.
 * Also, it decreases the emotion intensities according to ED_DEC_RATE.
 * Finally, detectAndDraw is called.
 ********************/
int Emodetect::update()
{
	const clock_t clk = clock();

    #ifdef _DEBUG
    std::cout << ">>> Entering edUpdate..." << std::endl;
    #endif


	{
		// we aim for 10 updates per second each with geometric decay of 0.9
		// so cast ms difference onto a timescale of 0.1*seconds (wiktionary says: 'deciseconds')
		float dT = (float)(clk - mLastUpdate) / 100;
		float rate = std::pow(ED_DEC_RATE, dT);
#ifdef _DEBUG
		std::cout << "      > Decreasing Emotion values with ED_DEC_RATE=";
		std::cout << rate << "..." << std::endl;
#endif
		mHappy = (int)(mHappy*rate);
		mSad *= (int)(mSad*rate);
		mAnger *= (int)(mAnger*rate);
		mFear *= (int)(mFear*rate);
		mLastUpdate = clk;
	}

    if (!mCapture)
    {
        std::cerr << "ERROR: update impossible, no capture found!" << std::endl;
        return ERROR_CAPTURE;
    }

    #ifdef _DEBUG
    std::cout << "      > Getting frames..." << std::endl;
    #endif

	cv::Mat frameCopy;
	int err = getNewFrame(frameCopy);
	if (err) return err;

    // detect emotions and draw picture
	detect(frameCopy);

	// Draw rectangles on the left side of the image reflecting intensities
	if (mDrawImage)
	{
		draw(frameCopy);
	}

    #ifdef _DEBUG
    std::cout << "<<< Exiting edUpdate. Used time: ";
    std::cout << float( clock() - clk ) / CLOCKS_PER_SEC << std::endl;
    #endif

    return 0;
}

/********************
 * This function calibrates the division value for the formula, with
 * which the min neighbor value for emotiondetect is calculated.
 * If that value is too high, emotions won't get detected. If it is
 * too low, the amount of false detections will increase.
 * The min formula is calculated with the macro ED_MINCALC.
 * While calibration is running, try not to smile :)
 ********************/
void Emodetect::calibrate()
{

#ifdef _DEBUG
	std::cout << ">>> Entering edCalibrateMin..." << std::endl;
	const clock_t clk = clock();
	std::cout << "*****************************\n";
	std::cout << "* Calibration starting....  *\n";
	std::cout << "*****************************" << std::endl;
#endif

	switch (mCalib)
	{
	case CA_IMAGE_WIDTH:
	{
		std::cout << "* Try not to show emotions. *\n";
		const int steps = 10;
		const double dec = 0.8;
		const int thresh = 20;
		int i;

		mMindiv = DEFAULT_ED_MINDIV;

		do {
			// bypass slow decrease rate
			mHappy = 0;
			mSad = 0;
			mAnger = 0;
			mFear = 0;

			// get some frames to see how high emotion intensities are
			for (i = 0; i<steps; i++)
				update();

			// decrease ed_mindiv, so min will increase
			mMindiv = (int)(mMindiv*dec);
		} while (
			(mHappy>thresh) ||
			(mSad>thresh) ||
			(mAnger>thresh) ||
			(mFear>thresh));
		break;
	}
	case CA_CALIBRATED_BORDERS:
	{
		if (!mCapture)
		{
			std::cerr << "ERROR: update impossible, no capture found!" << std::endl;
			return;
		}

		std::cout << "* Try not to show emotions. *\n";
		{ // stat based minimum neighbour find
			std::deque<unsigned int> buffer;
			for (int i = 0; i < 10; ++i) buffer.push_back(rand() % 1000);
			for (;;)
			{
				cv::Mat frameCopy;
				getNewFrame(frameCopy);
				unsigned int neighbours = detectOnlyNeighbours(frameCopy);
				buffer.push_back(neighbours);

				double mean, variance;
				getStandardStatistic(buffer, mean, variance);
				buffer.pop_front();

				if (variance < 40)
				{
					mMinNeighbours = (int)mean;
					break;
				}
			}
		}

		std::cout << "* Smile as hard as you can. *\n";
		{ // stat based maximum  neighbour find
			std::deque<unsigned int> buffer;
			for (int i = 0; i < 10; ++i) buffer.push_back(rand() % 1000);
			for (;;)
			{
				cv::Mat frameCopy;
				getNewFrame(frameCopy);
				unsigned int neighbours = detectOnlyNeighbours(frameCopy);
				buffer.push_back(neighbours);

				double mean, variance;
				getStandardStatistic(buffer, mean, variance);
				buffer.pop_front();

				if (variance < 40)
				{
					mMaxNeighbours = (int)mean;
					break;
				}
			}
		}
		break;
	}
	}

#ifdef _DEBUG
	std::cout << "        var ed_mindiv: " << mMindiv << std::endl;
	std::cout << "<<< Exiting edCalibrateMin. Used time: ";
	std::cout << float(clock() - clk) / CLOCKS_PER_SEC << std::endl;
	std::cout << "*********************\n";
	std::cout << "* Calibration done. *\n";
	std::cout << "*********************" << std::endl;
#endif
	mNeedsCalibration = false;
}

