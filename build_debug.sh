#!/bin/bash

cd build && rm -rf * && cmake -DCMAKE_BUILD_TYPE=Debug -DQt5Widgets_DIR=/opt/Qt5.2.1/5.2.1/gcc_64/lib/cmake/Qt5Widgets -DQt5PrintSupport_DIR=/opt/Qt5.2.1/5.2.1/gcc_64/lib/cmake/Qt5PrintSupport -DQt5Network_DIR=/opt/Qt5.2.1/5.2.1/gcc_64/lib/cmake/Qt5Network .. && make
