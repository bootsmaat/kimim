#ifndef KIMIM_H
#define KIMIM_H

#include <QMainWindow>
#include <QFile>
#include <QPrinter>
#include "ui_kimim.h"
#include "ui_calibrateDialog.h"
#include "emodetect.h"
#include "emodetectthread.h"
#include "emailprovider.h"

class CalibrateDialog : public QDialog
{
	Q_OBJECT

public:
	CalibrateDialog(QWidget *parent = 0);
	~CalibrateDialog();

public slots:
	void ok();

private:
	int mState;
	Ui::CalibrateDialog ui;
};

class Kimim : public QMainWindow
{
	Q_OBJECT

public:
	Kimim(QWidget *parent = 0);
	~Kimim();

public slots:
	void quit();

	// change font to match current emotion from [0,99]
	void initEmotion();
	void updateEmotion();
	void setHappy(int happy);
	void calibrate();
	void record();

	// toggle the slidebar
	void toggleExtraToolbars();

	// importers
	void htmlImport();

	// exporters
	void pdfExport();
	void htmlExport();
	void print();

private:
	void preloadFonts();
	void setupPrinters();

	std::vector<QString> mFontFamilies;
	QPrinter* mPrinter;
	QTimer* mEmotionUpdateTimer;

private:
	EmodetectThread* mDetectorThread;
	EmailProvider* mEmail;

	CalibrateDialog* mCalibDialog;

	int mCurrentHappy;
	Ui::KimimClass ui;
};

#endif // KIMIM_H
