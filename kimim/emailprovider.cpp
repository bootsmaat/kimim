#include "emailprovider.h"
#include <QHostInfo>
#include <QTextCodec>
#include <QFileInfo>
#include <time.h>

EmailProvider::EmailProvider(const QString &smtpServer, int port)
{
	setSmtpServer(smtpServer);
	setPort(port);
	setTimeout(30000);
	setPriority(normal);
	setContentType(text);
	setEncoding(_7bit);
	setISO(utf8);
}
EmailProvider::~EmailProvider()
{
	if (mSocket) delete mSocket;
}

static QString encode64(QString xml)
{
	QByteArray text;
	text.append(xml);
	return text.toBase64();
}
static QString createBoundary()
{
	QByteArray hash = QCryptographicHash::hash(QString(QString::number(qrand())).toUtf8(), QCryptographicHash::Md5);
	QString boundary = hash.toHex();
	boundary.truncate(26);
	boundary.prepend("----=_NextPart_");
	return boundary;
}
static int dateswap(QString form, uint unixtime)
{
	QDateTime fromunix;
	fromunix.setTime_t(unixtime);
	QString numeric = fromunix.toString((const QString)form);
	bool ok;
	return (int)numeric.toFloat(&ok);
}
static QString TimeStampMail()
{
	/* mail rtf Date format! http://www.faqs.org/rfcs/rfc788.html */
	uint unixtime = (uint)time(NULL);
	QDateTime fromunix;
	fromunix.setTime_t(unixtime);
	QStringList RTFdays = QStringList() << "giorno_NULL" << "Mon" << "Tue" << "Wed" << "Thu" << "Fri" << "Sat" << "Sun";
	QStringList RTFmonth = QStringList() << "mese_NULL" << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun" << "Jul" << "Aug" << "Sep" << "Oct" << "Nov" << "Dec";
	QDate timeroad(dateswap("yyyy", unixtime), dateswap("M", unixtime), dateswap("d", unixtime));
	QStringList rtfd_line;
	rtfd_line.clear();
	rtfd_line.append("Date: ");
	rtfd_line.append(RTFdays.at(timeroad.dayOfWeek()));
	rtfd_line.append(", ");
	rtfd_line.append(QString::number(dateswap("d", unixtime)));
	rtfd_line.append(" ");
	rtfd_line.append(RTFmonth.at(dateswap("M", unixtime)));
	rtfd_line.append(" ");
	rtfd_line.append(QString::number(dateswap("yyyy", unixtime)));
	rtfd_line.append(" ");
	rtfd_line.append(fromunix.toString("hh:mm:ss"));
	rtfd_line.append("");
	return QString(rtfd_line.join(""));
}
static QString getMimeType(QString ext)
{
	//texts
	if (ext == "txt")            return "text/plain";
	if (ext == "htm" || ext == "html")    return "text/html";
	if (ext == "css")            return "text/css";
	//Images
	if (ext == "png")            return "image/png";
	if (ext == "gif")            return "image/gif";
	if (ext == "jpg" || ext == "jpeg")    return "image/jpeg";
	if (ext == "bmp")            return "image/bmp";
	if (ext == "tif")            return "image/tiff";
	//Archives
	if (ext == "bz2")            return "application/x-bzip";
	if (ext == "gz")            return "application/x-gzip";
	if (ext == "tar")            return "application/x-tar";
	if (ext == "zip")            return "application/zip";
	//Audio
	if (ext == "aif" || ext == "aiff")    return "audio/aiff";
	if (ext == "mid" || ext == "midi")    return "audio/mid";
	if (ext == "mp3")            return "audio/mpeg";
	if (ext == "ogg")            return "audio/ogg";
	if (ext == "wav")            return "audio/wav";
	if (ext == "wma")            return "audio/x-ms-wma";
	//Video
	if (ext == "asf" || ext == "asx")    return "video/x-ms-asf";
	if (ext == "avi")            return "video/avi";
	if (ext == "mpg" || ext == "mpeg")    return "video/mpeg";
	if (ext == "wmv")            return "video/x-ms-wmv";
	if (ext == "wmx")            return "video/x-ms-wmx";
	//XML
	if (ext == "xml")            return "text/xml";
	if (ext == "xsl")            return "text/xsl";
	//Microsoft
	if (ext == "doc" || ext == "rtf")    return "application/msword";
	if (ext == "xls")            return "application/excel";
	if (ext == "ppt" || ext == "pps")    return "application/vnd.ms-powerpoint";
	//Adobe
	if (ext == "pdf")            return "application/pdf";
	if (ext == "ai" || ext == "eps")    return "application/postscript";
	if (ext == "psd")            return "image/psd";
	//Macromedia
	if (ext == "swf")            return "application/x-shockwave-flash";
	//Real
	if (ext == "ra")            return "audio/vnd.rn-realaudio";
	if (ext == "ram")            return "audio/x-pn-realaudio";
	if (ext == "rm")            return "application/vnd.rn-realmedia";
	if (ext == "rv")            return "video/vnd.rn-realvideo";
	//Other
	if (ext == "exe")            return "application/x-msdownload";
	if (ext == "pls")            return "audio/scpls";
	if (ext == "m3u")            return "audio/x-mpegurl";
	return "text/plain"; // default
}

void EmailProvider::setFrom(const QString &from)
{
	mFrom = from;
	mFromName = from;
	mReplyTo = from;
}
void EmailProvider::setISO(ISO iso)
{
	switch (iso)
	{
	case iso88591:
	{
		mISO = "iso-8859-1"; mBodyCodec = "ISO 8859-1"; break;
	}
	case utf8:
	{
		mISO = "utf-8"; mBodyCodec = "UTF-8"; break;
	}
	}
}
void EmailProvider::setEncoding(Encoding encoding)
{
	switch (encoding)
	{
	case _7bit:
	{
		mEncoding = "7bit";
		break;
	}
	case _8bit:
	{
		mEncoding = "8bit";
		break;
	}
	case base64:
	{
		mEncoding = "base64";
		break;
	}
	}
}

QString EmailProvider::mailData()
{
	QString data;

	QString boundary1 = createBoundary();
	QString boundary2 = createBoundary();

	QByteArray hash = QCryptographicHash::hash(QString(QString::number(qrand())).toUtf8(), QCryptographicHash::Md5);
	QString id = hash.toHex();
	data.append("Message-ID: " + id + "@" + QHostInfo::localHostName() + "\n");

	data.append("From: \"" + mFromName + "\"");
	data.append(" <" + mFrom + ">\n");

	if (mTo.count() > 0)
	{
		data.append("To: ");
		for (int i = 0; i < mTo.count(); ++i)
		{
			data.append("<" + mTo.at(i) + ">" + ",");
		}
		data.append("\n");
	}
	if (mCc.count() > 0)
	{
		data.append("Cc: ");
		for (int i = 0; i < mTo.count(); ++i)
		{
			data.append("<" + mTo.at(i) + ">");
			if (i < mCc.count() - 1)
				data.append(",");
		}
		data.append("\n");
	}

	data.append("Subject: " + mSubject + "\n");
	data.append(TimeStampMail() + "\n");

	data.append("MIME-Version: 1.0\n");
	data.append("Content-Type: Multipart/Mixed; boundary=\"" + boundary1 + "\"\n");

	switch (mPriority)
	{
	case low:
		data.append("X-Priority: 5\n");
		data.append("Priority: Non-Urgent\n");
		data.append("X-MSMail-Priority: Low\n");
		data.append("Importance: low\n");
		break;
	case high:
		data.append("X-Priority: 1\n");
		data.append("Priority: Urgent\n");
		data.append("X-MSMail-Priority: High\n");
		data.append("Importance: high\n");
		break;
	default:
		data.append("X-Priority: 3\n");
		data.append("    X-MSMail-Priority: Normal\n");
	}
	data.append("X-Mailer: QT5\r\n");

	if (!mConfirmTo.isEmpty()) data.append("Disposition-Notification-To: " + mConfirmTo + "\n");
	if (!mReplyTo.isEmpty() && mConfirmTo != mFrom)
	{
		data.append("Reply-to: " + mReplyTo + "\n");
		data.append("Return-Path: <" + mReplyTo + ">\n");
	}

	data.append("\n");

	data.append("This is a multi-part message in MIME format.\r\n\n");
	data.append("--" + boundary1 + "\n");
	data.append("Content-Type: multipart/alternative; boundary=\"" + boundary2 + "\"\n\n\n");
	data.append("--" + boundary2 + "\n");

	data.append("Content-Type: " + contentType() + ";\n");
	data.append("  charset=" + mISO + "\r\n");
	data.append("Content-Transfer-Encoding: " + mEncoding + "\r\n");
	data.append("\r\n\n");

	if (mContentType == html)
	{
		data.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n");
		data.append("<HTML><HEAD>\r\n");
		data.append("<META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=" + mISO + "\">\r\n");
		data.append("<META content=\"MSHTML 6.00.2900.2802\" name=GENERATOR>\r\n");
		data.append("<STYLE></STYLE>\r\n");
		data.append("</head>\r\n");
		data.append("<body bgColor=#ffffff>\r\n");
	}

	QByteArray encodedBody(mBody.toUtf8());//toLatin1()); // = array;
	QTextCodec *codec = QTextCodec::codecForName(mBodyCodec.toUtf8());//toLatin1());
	data.append(codec->toUnicode(encodedBody) + "\r\n");

	if (mContentType == html)
	{
		data.append("<DIV>&nbsp;</DIV></body></html>\r\n\n");
		data.append("--" + boundary2 + "--\r\n\n");
	}

	if (mAttachments.count() > 0)
	{
		for (int i = 0; i < mAttachments.count(); i++)
		{
			data.append("--" + boundary1 + "\n");
			QFileInfo fileinfo(mAttachments.value(i));
			QString type = getMimeType(fileinfo.suffix());
			data.append("Content-Type: " + type + ";\n");
			data.append("  name=" + fileinfo.fileName() + "\n");

			QString tt = fileinfo.fileName();
			data.append("Content-Transfer-Encoding: base64\n");
			data.append("Content-Disposition: attachment\n");
			data.append("  filename=" + fileinfo.fileName() + "\n\n");
			QString encodedFile;
			QString f = mAttachments.value(i);
			QFile file(mAttachments.value(i));
			file.open(QIODevice::ReadOnly);
			QDataStream in(&file);
			quint8 a;
			char c;
			QString b;
			while (!in.atEnd())
			{
				in >> a;
				c = a;
				b.append(c);
			}
			encodedFile = encode64(b);
			data.append(encodedFile);
			data.append("\r\n\n");
		}
		data.append("--" + boundary1 + "--\r\n\n");
	}

	mLastMailData = data;
	return data;
}

bool EmailProvider::send()
{
	mLastError = "";

	if (mSocket) delete mSocket;

	mSocket = mSsl ? new QSslSocket(this) : new QTcpSocket(this);
	connect(mSocket, SIGNAL(error(QAbstractSocket::SocketError)),
		this, SLOT(errorReceived(QAbstractSocket::SocketError)));
	connect(mSocket, SIGNAL(proxyAuthenticationRequired(const QNetworkProxy &, QAuthenticator *)),
		this, SLOT(proxyAuthentication(const QNetworkProxy &, QAuthenticator *)));

	bool auth = !mLogin.isEmpty();

	mSocket->connectToHost(mSmtpServer, mPort);

	if (!mSocket->waitForConnected(mTimeOut))
	{
		error("Timeout connecting to host");
		return false;
	}

	if (!read("220"))  return false;

	if (!sendCommand("EHLO there", "250"))
	{
		if (!sendCommand("HELO there", "250"))
		{
			return false;
		}
	}

	if (mSsl)
	{
		if (!sendCommand("STARTTLS", "220")) return false;

		QSslSocket* ssl = qobject_cast<QSslSocket*>(mSocket);
		if (!ssl)
		{
			error("internal error casting to QSslSocket");
			return false;
		}
		ssl->startClientEncryption();
	}

	if (auth)
	{
		if (!sendCommand("AUTH LOGIN", "334")) return false;
		if (!sendCommand(encode64(mLogin), "334")) return false;
		if (!sendCommand(encode64(mPass), "235")) return false;
	}

	if (!sendCommand(QString::fromLatin1("MAIL FROM:<") + mFrom + QString::fromLatin1(">"), "250")) return false;

	QStringList recipients = mTo + mCc + mBcc;
	for (int i = 0; i < recipients.count(); ++i)
	{
		if (!sendCommand(QString::fromLatin1("RCPT TO:<") + recipients.at(i) + QString::fromLatin1(">"), "250")) return false;
	}

	if (!sendCommand(QString::fromLatin1("DATA"), "354")) return false;
	if (!sendCommand(mailData() + QString::fromLatin1("\r\n."), "250")) return false;
	if (!sendCommand(QString::fromLatin1("QUIT"), "221")) return false;

	mSocket->disconnectFromHost();
	return true;
}

bool EmailProvider::read(const QString &waitfor)
{
	if (!mSocket->waitForReadyRead(mTimeOut))
	{
		error("read timeout");
		return false;
	}
	if (!mSocket->canReadLine())
	{
		error("can't read");
		return false;
	}

	QString responseLine;
	do
	{
		responseLine = mSocket->readLine();
	} while (mSocket->canReadLine() && responseLine[3] != ' ');

	mLastResponse = responseLine;
	QString prefix = responseLine.left(3);
	bool isOk = (prefix == waitfor);
	if (!isOk)
	{
		error("waiting for " + waitfor + ", received " + prefix);
	}
	return isOk;
}
bool EmailProvider::sendCommand(const QString &cmd, const QString &waitfor)
{
	QTextStream t(mSocket);
	t << cmd + "\r\n";
	t.flush();

	mLastCommand = cmd;
	return read(waitfor);
}

QString EmailProvider::contentType()
{
	switch (mContentType)
	{
	case html:
	{
		return "text/html";
	}
	case multipartmixed:
	{
		return "multipart/mixed";
	}
	case text:
	default:
	{
		return "text/plain";
	}
	}
}

void EmailProvider::error(const QString &err)
{
	mLastError = err;
}
void EmailProvider::setProxyAuthenticator(const QAuthenticator &authenticator)
{
	mAuthenticator = authenticator;
}
void EmailProvider::errorReceived(QAbstractSocket::SocketError socketError)
{
	QString msg;
	switch (socketError)
	{
	case QAbstractSocket::ConnectionRefusedError: msg = "ConnectionRefusedError"; break;
	case QAbstractSocket::RemoteHostClosedError: msg = "RemoteHostClosedError"; break;
	case QAbstractSocket::HostNotFoundError: msg = "HostNotFoundError"; break;
	case QAbstractSocket::SocketAccessError: msg = "SocketAccessError"; break;
	case QAbstractSocket::SocketResourceError: msg = "SocketResourceError"; break;
	case QAbstractSocket::SocketTimeoutError: msg = "SocketTimeoutError"; break;
	case QAbstractSocket::DatagramTooLargeError: msg = "DatagramTooLargeError"; break;
	case QAbstractSocket::NetworkError: msg = "NetworkError"; break;
	case QAbstractSocket::AddressInUseError: msg = "AddressInUseError"; break;
	case QAbstractSocket::SocketAddressNotAvailableError: msg = "SocketAddressNotAvailableError"; break;
	case QAbstractSocket::UnsupportedSocketOperationError: msg = "UnsupportedSocketOperationError"; break;
	case QAbstractSocket::ProxyAuthenticationRequiredError: msg = "ProxyAuthenticationRequiredError"; break;
	default: msg = "Unknown Error";
	}
	error("Socket error [" + msg + "]");
}
void EmailProvider::proxyAuthentication(const QNetworkProxy &, QAuthenticator * authenticator)
{
	*authenticator = mAuthenticator;
}