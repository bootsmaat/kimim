#ifndef EMODETECT_THREAD_H
#define EMODETECT_THREAD_H

#include <QtCore>
#include "emodetect.h"

class EmodetectThread : public QThread
{
public:
    EmodetectThread(int updateDelta);
	
    volatile int Happy;

	void stop();
	void invokeCalibrate();
#ifdef _DEBUG
public:
	bool record();

private:
	bool mRecord;
	std::vector<int> mValues;

#endif


private:
	QMutex mMutex;
	bool mStop;
	bool mCalib;

	ed::Emodetect* mDetector;
    int mUpdateDelta;

	void run();
};

#endif // EMODETECT_THREAD_H
