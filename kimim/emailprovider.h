#ifndef EMAIL_PROVIDER_H
#define EMAIL_PROVIDER_H

#include <QString>
#include <QPointer>
#include <QDebug>

#include <QTcpSocket>
#include <QSslSocket>
#include <QAuthenticator>

// http://forum.qtfr.org/viewtopic.php?id=7736
class EmailProvider : public QObject
{
	Q_OBJECT

public:
	enum Priority
	{
		high,
		normal,
		low
	};
	enum ContentType
	{
		text,
		html,
		multipartmixed
	};
	enum Encoding
	{
		_7bit,
		_8bit,
		base64
	};
	enum ISO
	{
		utf8,
		iso88591
	};

	EmailProvider(const QString &smtpServer, int port);
	~EmailProvider();

	QString lastError() { return mLastError; }

	void setSmtpServer(const QString& smtpServer) { mSmtpServer = smtpServer; }
	void setPort(int port) { mPort = port; }
	void setTimeout(int timeout) { mTimeOut = timeout; }
	void setSsl(bool ssl) { mSsl = ssl; }
	void setLogin(const QString& login, const QString& pass) { mLogin = login; mPass = pass; }

	void setFrom(const QString& from, const QString& fromName) { mFrom = from; mFromName = fromName; }
	void setFrom(const QString &from);
	void setTo(const QStringList &to) { mTo = to; }
	void setCc(const QStringList &cc) { mCc = cc; }
	void setBcc(const QStringList &bcc) { mBcc = bcc; }
	void setSubject(const QString& subject) { mSubject = subject; }
	void setBody(const QString& body) { mBody = body; }
	void setAttachments(const QStringList &attachments) { mAttachments = attachments; }

	void setPriority(Priority priority) { mPriority = priority; }
	void setContentType(ContentType contentType) { mContentType = contentType; }
	void setISO(ISO iso);
	void setEncoding(Encoding encoding);
	void setProxyAuthenticator(const QAuthenticator &authenticator);

	bool send();

	private slots:
	void errorReceived(QAbstractSocket::SocketError socketError);
	void proxyAuthentication(const QNetworkProxy &proxy, QAuthenticator *authenticator);

private:
	void error(const QString &err);
	QString mailData();
	bool read(const QString &waitfor);
	bool sendCommand(const QString &cmd, const QString &waitfor);
	QString contentType();

	QPointer<QTcpSocket> mSocket;
	QAuthenticator mAuthenticator;

	QString mSmtpServer;
	int mPort;
	int mTimeOut;
	bool mSsl;
	QString mLogin;
	QString mPass;

	QString mFrom;
	QString mFromName;
	QStringList mTo;
	QStringList mCc;
	QStringList mBcc;
	QString mSubject;
	QString mBody;
	QStringList mAttachments;

	Priority mPriority;
	ContentType mContentType;
	QString mEncoding;
	QString mISO;
	QString mBodyCodec;

	QString mConfirmTo;
	QString mReplyTo;

	QString mLastError;
	QString mLastResponse;
	QString mLastCommand;
	QString mLastMailData;
};

#endif