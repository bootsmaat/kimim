#include "kimim.h"
#include <sstream>
#include <QFontDatabase>
#include <QFileDialog>
#include <QPrintDialog>
#include <QTextStream>
#include <QTimer>

#define FONT_PATH "../../fonts/FranklinSlanted-"
#define FONT_EXTENSION ".otf"

CalibrateDialog::CalibrateDialog(QWidget *parent)
{
	ui.setupUi(this);
	connect(ui.pushButton, SIGNAL(clicked(bool)), this, SLOT(ok()));
	mState = 0;
}
CalibrateDialog::~CalibrateDialog()
{

}
void CalibrateDialog::ok()
{
	if (mState == 0)
	{
		ui.label->setText(QString("Entspanne nun dein Gesicht und<br/> dr�cke erneut auf Ok!"));
		mState = 1;
	}
}


Kimim::Kimim(QWidget *parent)
: QMainWindow(parent)
{
	ui.setupUi(this);

	preloadFonts();
	setupPrinters();

	initEmotion();

	connect(ui.sldSlant, SIGNAL(valueChanged(int)), this, SLOT(setHappy(int)));
	connect(ui.btnPDFExport, SIGNAL(triggered()), this, SLOT(pdfExport()));
	connect(ui.btnImportHtml, SIGNAL(triggered()), this, SLOT(htmlImport()));
	connect(ui.btnHtmlExport, SIGNAL(triggered()), this, SLOT(htmlExport()));
	connect(ui.btnPrint, SIGNAL(triggered()), this, SLOT(print()));
	connect(ui.btnToggleExtraToolbars, SIGNAL(triggered()), this, SLOT(toggleExtraToolbars()));
	connect(ui.btnCalibrate, SIGNAL(triggered()), this, SLOT(calibrate()));
	connect(ui.btnQuit, SIGNAL(triggered()), this, SLOT(quit()));
	ui.sldSlant->setVisible(false);

	mEmotionUpdateTimer = new QTimer(this);
	connect(mEmotionUpdateTimer, SIGNAL(timeout()), this, SLOT(updateEmotion()));
	connect(ui.btnRecord, SIGNAL(clicked(bool)), this, SLOT(record()));

	mDetectorThread->start();
	mEmotionUpdateTimer->start(100);

	/* Email test. Insert your login information!
	mEmail = new EmailProvider("smtp.gmail.com", 587);
	mEmail->setLogin("username", "pass");

	mEmail->setFrom("schuler.maximilian@gmail.com");
	mEmail->setTo({ "schuler.maximilian@gmail.com" });
	mEmail->setBody("test body");
	mEmail->setSubject("test subject");
	if (!mEmail->send())
	{
		QString error = mEmail->lastError();
	}
	*/

	mCalibDialog = new CalibrateDialog(this);
	mCalibDialog->show();
}

Kimim::~Kimim()
{
	if (mPrinter) delete mPrinter;
	mPrinter = 0;
	if (mEmotionUpdateTimer) delete mEmotionUpdateTimer;
	mEmotionUpdateTimer = 0;
	if (mDetectorThread)
	{
		mDetectorThread->stop();
		while (mDetectorThread->isRunning());
		mDetectorThread->terminate();
		delete mDetectorThread;
	}
	mDetectorThread = 0;
}

void Kimim::quit()
{
	QApplication::quit();
}

void Kimim::initEmotion()
{
	mDetectorThread = new EmodetectThread(500);
	mCurrentHappy = ui.sldSlant->sliderPosition();
	setHappy(mCurrentHappy);
}
void Kimim::updateEmotion()
{
	//mDetector->update();
	//_sleep(1000);
	ui.sldSlant->setValue(mDetectorThread->Happy);
}
void Kimim::setHappy(int happy)
{
	happy = std::max(0, std::min(99, happy));

	if (happy != mCurrentHappy)
	{
		QTextCursor cursor = ui.textEdit->textCursor();
		if (!cursor.hasSelection())
		{
			QFont f(mFontFamilies[happy]);
			f.setPointSize(18);
			ui.textEdit->setCurrentFont(f);
			mCurrentHappy = happy;
		}
	}
}
void Kimim::calibrate()
{
	mDetectorThread->invokeCalibrate();
}
void Kimim::record()
{
#ifdef _DEBUG
	bool recording = mDetectorThread->record();
	ui.btnRecord->setText(recording ? "Recording..." : "Start Recording");
#endif
}


void Kimim::toggleExtraToolbars()
{
	ui.sldSlant->setVisible(!ui.sldSlant->isVisible());
}

void Kimim::pdfExport()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Pdf File"),
		"", tr("Files (*.pdf)"));

	mPrinter->setOutputFileName(fileName);
	ui.textEdit->print(mPrinter);
}
void Kimim::htmlExport()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Html File"),
		"", tr("Files (*.html)"));

	QFile file(fileName);
	if (file.open(QIODevice::ReadWrite))
	{
		QTextStream stream(&file);
		stream << ui.textEdit->toHtml();
		file.close();
	}
}
void Kimim::htmlImport()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Html File"),
		"", tr("Files (*.html)"));

	QFile file(fileName);
	if (file.open(QIODevice::ReadWrite))
	{
		ui.textEdit->setHtml(file.readAll());
		file.close();
	}
}
void Kimim::print()
{
	QPrinter printer;

	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (dlg->exec() != QDialog::Accepted)
		return;

	ui.textEdit->print(&printer);
}


void Kimim::preloadFonts()
{
	QFontDatabase database;
	for (int i = 0; i < 100; ++i)
	{
		std::stringstream ss;
		ss << FONT_PATH << i << FONT_EXTENSION;
		std::string filename = ss.str();
		int id = database.addApplicationFont(filename.c_str());
        if (id == -1)
          std::cerr << "Warning: Could not load font '" << ss << "!\n" << endl;
		QStringList family = database.applicationFontFamilies(id);
		mFontFamilies.push_back(family.first());
	}
}
void Kimim::setupPrinters()
{
	mPrinter = new QPrinter(QPrinter::HighResolution);
	mPrinter->setOrientation(QPrinter::Portrait);
	mPrinter->setPageSize(QPrinter::A4);
	mPrinter->setResolution(72);
	mPrinter->setOutputFormat(QPrinter::PdfFormat);
	mPrinter->setOutputFileName("");
	mPrinter->newPage();

	QRectF pageRect(mPrinter->pageRect());
	QRect body = QRect(0, 0, pageRect.width(), pageRect.height());
	ui.textEdit->document()->setPageSize(body.size());
	ui.textEdit->setFixedSize(body.size());
}
