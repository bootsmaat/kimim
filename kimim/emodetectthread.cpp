#include "emodetectthread.h"
#include <fstream>

EmodetectThread::EmodetectThread(int updateDelta)
{
    mUpdateDelta = updateDelta;
	mStop = false;
	mCalib = false;
#ifdef _DEBUG
	mRecord = false;
#endif
}


#ifdef _DEBUG

bool EmodetectThread::record()
{
	QMutexLocker locker(&mMutex);

	if (!mRecord)
	{
		std::cout << "start recording" << std::endl;
		mRecord = true;
	}
	else
	{
		mRecord = false;
		std::cout << "writing data " << mValues.size() << " values" << std::endl;
		std::ofstream file("data.csv", std::ofstream::out | std::ofstream::app);
		std::vector<int>::const_iterator iter, iend;
		iter = mValues.begin(); iend = mValues.end();

		for (; iter != iend;)
		{
			file << *iter;
			++iter;
			iter != iend ?
				file << "," :
				file << std::endl;
		}

		file.close();
		mValues.clear();
	}
	return mRecord;
}

#endif


void EmodetectThread::stop()
{
	QMutexLocker locker(&mMutex);
	mStop = true;
}
void EmodetectThread::invokeCalibrate()
{
	QMutexLocker locker(&mMutex);
	mCalib = true;
}
void EmodetectThread::run()
{
    mDetector = new ed::Emodetect();

#ifdef _DEBUG
    mDetector->init(true);
#else
    mDetector->init();
#endif

    QTime timer;
	for(;;)
	{
		QMutexLocker locker(&mMutex);
        timer.start();
        {
			if (mCalib)
			{
				mDetector->calibrate();
				mCalib = false;
			}
#ifdef _DEBUG
			else if (mRecord)
			{
				mDetector->update();
				mValues.push_back(mDetector->getNeighbours());
			}
#endif
            else
            {
				mDetector->update();
            }
            Happy = mDetector->getHappy();
        }
		if(mStop)
			break;
        int pastMs = timer.elapsed();
        int remainingMs = mUpdateDelta - pastMs;
        if(remainingMs > 0)
            msleep(remainingMs);
	}

	mDetector->destroy();
}
